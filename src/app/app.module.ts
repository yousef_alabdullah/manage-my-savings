import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SetTargetComponent } from './set-target/set-target.component';
import { FormsModule } from '@angular/forms';
import { TargetServiceService } from './target-service.service';
import { AddAmountComponent } from './add-amount/add-amount.component';
import { AccountService } from './account.service';
import { TrackProgressComponent } from './track-progress/track-progress.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavBarComponent,
    SetTargetComponent,
    AddAmountComponent,
    TrackProgressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [TargetServiceService, AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
