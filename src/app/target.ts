export class Target
{
    amount: number;
    duration: Duration;
}

export enum Duration
{
    Daily, Weekly, Monthly, Yearly
}