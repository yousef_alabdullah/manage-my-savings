import { Component, OnInit } from '@angular/core';
import { TargetServiceService } from '../target-service.service';
import { AccountService } from '../account.service';
import { Target, Duration } from '../target';



@Component({
  selector: 'app-track-progress',
  templateUrl: './track-progress.component.html',
  styleUrls: ['./track-progress.component.css']
})
export class TrackProgressComponent implements OnInit {



  target: Target;
  currentProgress: number;

  remainingTime: number;

  percentage: number;

  constructor(private targetService: TargetServiceService, private accountService:AccountService) { }

  ngOnInit() 
  {
    this.target = this.targetService.getTarget();
    this.currentProgress = this.accountService.getAccountSum();

    this.percentage = (100 * this.currentProgress) / this.target.amount;

    switch (this.target.duration)
    {
      case Duration.Daily:
        this.calculateHoursRemaining();
        break;
      case Duration.Weekly:
        this.calculateDaysRemaining();
        break;
      case Duration.Monthly:
        this.calculateDaysForMonthRemaining();
        break;
      case Duration.Yearly:
        this.calculateMonthsForYearlyRemaining();
        break;
    }

    console.log(this.target);
    console.log(this.currentProgress);
    console.log(this.percentage);
    
  }

  calculateMonthsForYearlyRemaining(): void 
  {
    let today = new Date(Date.now());
    let nextMonth = new Date(Date.now());
    nextMonth.setDate(1);
    nextMonth.setFullYear(nextMonth.getFullYear());
    

  }

  calculateHoursRemaining(): any {
    throw new Error("Method not implemented.");
  }
  calculateDaysForMonthRemaining(): any {
    throw new Error("Method not implemented.");
  }

  calculateWeeksRemaining(): void 
  {
    throw new Error("Method not implemented.");
  }

  calculateDaysRemaining(): void 
  {
    throw new Error("Method not implemented.");
  }


}
