import { Injectable } from '@angular/core';
import { Transaction } from './transaction';
import { forEach } from '@angular/router/src/utils/collection';

@Injectable()
export class AccountService {

  transactions: Transaction[] = [];
  totalAmount: number = 0;
  

  constructor() 
  {
    let c = this.getCookie('transactions');
    if (c !== '')
    {
      this.transactions = JSON.parse(c);
      this.transactions.forEach(t => this.totalAmount += t.amount);
      console.log(this.transactions);
    }
  }

  public getNextId(): number
  {
    return this.transactions.length + 1;
  }

  public addTransaction(transaction: Transaction): void
  {
    this.transactions.push(transaction);
    this.totalAmount += transaction.amount;
    this.setCookie('transactions', JSON.stringify(this.transactions), 365, 'manage-my-savings');
  }

  public getAccountSum(): number
  {
    return this.totalAmount;
  }

  private getCookie(name: string) {
    let ca: Array<string> = document.cookie.split(';');
    let caLen: number = ca.length;
    let cookieName = `${name}=`;
    let c: string;

    for (let i: number = 0; i < caLen; i += 1) {
        c = ca[i].replace(/^\s+/g, '');
        if (c.indexOf(cookieName) == 0) {
            return c.substring(cookieName.length, c.length);
        }
    }
    return '';
  }

  private deleteCookie(name) {
      this.setCookie(name, '', -1);
  }

  private setCookie(name: string, value: string, expireDays: number, path: string = '') {
      let d:Date = new Date();
      d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
      let expires:string = `expires=${d.toUTCString()}`;
      let cpath:string = path ? `; path=${path}` : '';
      document.cookie = `${name}=${value}; ${expires}${cpath}`;
  }

}
