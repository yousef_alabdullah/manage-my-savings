export class Transaction
{
    id: number;
    amount: number;
    date: Date;
}