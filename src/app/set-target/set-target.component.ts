import { Component, OnInit } from '@angular/core';
import { TargetServiceService } from '../target-service.service';
import { Duration, Target } from '../target';

@Component({
  selector: 'app-set-target',
  templateUrl: './set-target.component.html',
  styleUrls: ['./set-target.component.css']
})
export class SetTargetComponent implements OnInit {

  durations = Duration;

  keys(): Array <string>
  {
    var keys = Object.keys(this.durations);
    return keys.slice(keys.length / 2);
  }

  amount: number;
  duration: Duration;

  constructor(private targetService: TargetServiceService) { }

  ngOnInit() 
  {
    let t = this.targetService.getTarget();
    if (t != null)
    {
      this.amount = t.amount;
      this.duration = t.duration;
    }
  }

  setTarget(): void
  {
    if (!this.validData())
    {
      return;
    }
    let t = new Target();
    t.amount = this.amount;
    t.duration = this.duration;
    this.targetService.setTarget(t);
    console.log('New target was set');
    console.log(t);
  }

  validData(): boolean
  {
    return this.amount != null && this.amount > 0 && this.duration != null;
  }
}
