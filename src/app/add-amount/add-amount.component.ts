import { Component, OnInit } from '@angular/core';
import { Transaction } from '../transaction';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-add-amount',
  templateUrl: './add-amount.component.html',
  styleUrls: ['./add-amount.component.css']
})
export class AddAmountComponent implements OnInit {

  amount: number;
  paymentMethod: string;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
  }

  addAmount(): void
  {
    if (this.amount != null && this.amount > 0 && this.paymentMethod != null)
    {
      let transaction = new Transaction();
      transaction.amount = this.amount;
      transaction.date = new Date(Date.now());
      transaction.id = this.accountService.getNextId();
      this.accountService.addTransaction(transaction);


      //redirect to main
    }
  }

  changePaymentMethod(type: string): void
  {
    this.paymentMethod = type;
    console.log(this.paymentMethod);
  }

}
